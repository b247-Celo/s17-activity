/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
	function usersName() {
	alert("What is your name?");
	let firstNameandlastName = prompt("Enter your first name and last name:");
	let yourAge = prompt("Enter your age:");
	let yourCity = prompt("Enter your City:");
	console.log('Hello, ' + firstNameandlastName);
	console.log('You are ' + yourAge + ' years old');
	console.log('You live in  ' + yourCity);


	};
	usersName();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

	function favArtBand() {
		alert("Please enter your favorite artists/band in chronological order:");
		let firstArtBand = prompt("Enter your first favorite artist/band");
		let secondArtBand = prompt("Enter your second favorite artist/band");
		let thirdArtBand = prompt("Enter your third favorite artist/band");
		let fourthArtBand = prompt("Enter your fourth favorite artist/band");
		let fifthArtBand = prompt("Enter your fifth favorite artist/band");

		console.log('1. ' + firstArtBand)
		console.log('2. ' + secondArtBand)
		console.log('3. ' + thirdArtBand)
		console.log('4. ' + fourthArtBand)
		console.log('5. ' + fifthArtBand)
	};
		favArtBand()
/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

		function favMovies() {
		alert("Please enter your favorite movies in order:");
		let firstFavMovie = prompt("Enter your first favorite movie");
		let RTrating1 = prompt("Enter Rotten Tomatoes Rating");
		let secondFavMovie = prompt("Enter your second favorite movie");
		let RTrating2 = prompt("Enter Rotten Tomatoes Rating");
		let thirdFavMovie = prompt("Enter your third favorite movie");
		let RTrating3 = prompt("Enter Rotten Tomatoes Rating");
		let fourthFavMovie = prompt("Enter your fourth favorite movie");
		let RTrating4 = prompt("Enter Rotten Tomatoes Rating");
		let fifthFavMovie = prompt("Enter your fifth favorite movie");
		let RTrating5 = prompt("Enter Rotten Tomatoes Rating");

		console.log('1. ' + firstFavMovie);
		console.log('Rotten Tomatoes Rating: ' + RTrating1 + '%');
		console.log('2. ' + secondFavMovie);
		console.log('Rotten Tomatoes Rating: ' + RTrating2 + '%');
		console.log('3. ' + thirdFavMovie);
		console.log('Rotten Tomatoes Rating: ' + RTrating3 + '%');
		console.log('4. ' + fourthFavMovie);
		console.log('Rotten Tomatoes Rating: ' + RTrating4 + '%');
		console.log('5. ' + fifthFavMovie);
		console.log('Rotten Tomatoes Rating: ' + RTrating5 + '%');

	};

	favMovies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

printUsers();
function printUsers() {
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name: " ); 
	let friend2 = prompt("Enter your second friend's name: " ); 
	let friend3 = prompt("Enter your third friend's name: " );

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};



